#!/usr/bin/env python
# coding: utf-8

# In[38]:


get_ipython().run_line_magic('matplotlib', 'notebook')

from playsound import playsound
import _thread  

def play_bg():
    playsound('./draw.mp3')

_thread.start_new_thread(play_bg,())

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time

fig, ax = plt.subplots(figsize=(8,7))
x = np.arange(0, 2, 0.01)
sentence = ['Please sitdown!',
            'Start accelerate!',
            'Send you my litte heart hreart!',
            'Thank you!']

def animate(i):
    ax.clear()
    
    #if i < 10:
       # sleep(20)
        
    y = x**(2/3) + 0.9*np.sqrt(3.3-x**2)*np.sin(i*np.pi*x)-0.2
    plt.plot(x,  y, 'r', alpha=0.5)
    plt.plot(-x, y, 'r', alpha=0.5)
    
    if(i > 10):
        y = x**(2/3) + 0.9*np.sqrt(3.3-x**2)*np.sin((i-10)*np.pi*x)-0.2
        plt.plot(x*2/3,  y*2/3)
        plt.plot(-x*2/3, y*2/3)
        

    for spine in plt.gca().spines.values():
        spine.set_visible(False)
        
    plt.xlim([-2.5, 2.5])
    plt.ylim([-2.5, 2.5])
    plt.xticks([])
    plt.yticks([])
    
    index = int(i/10)
    if index > 2 and index < 6:
        index = 2
    elif index >= 6:
        index = 3
        
    plt.title(sentence[index], fontsize=14)

j = 10
frames = np.arange(0, j, 0.02)

for speed in np.arange(0.02, 0.08, 0.02):
    frames = np.append(frames, np.arange(j, j+3, speed))
    j += 3
    
for speed in np.arange(0.08, 0.02, -0.02):
    frames = np.append(frames, np.arange(j, j+2, speed))
    j += 2
print(j)
frames = np.append(frames, np.arange(j, 63, 0.02))
ani = animation.FuncAnimation(fig, animate, interval=40, frames=frames, repeat=False)

plt.show()

